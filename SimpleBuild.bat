@echo on
rem : make sure git modules are up to date
git submodule update --init

rem : do the build
SET PROJECT_PATH=%~dp0
SET BUILD_PATH=%PROJECT_PATH%Build/Win64.exe
SET BUILD_OPS=%PROJECT_PATH%buildops.log
SET TEST_OPS=%PROJECT_PATH%testops.log
SET TEST_RESULTS=%PROJECT_PATH%test_results.xml

if defined UNITY_VERSION goto :skip_version_parse
rem find out unity version
set VERSION_FILE=%PROJECT_PATH%ProjectSettings\ProjectVersion.txt

set /p VERSION_TEXT_LINE=< %VERSION_FILE% 
for /f "tokens=1,*" %%a in ("%VERSION_TEXT_LINE%") do set VERSION_REST=%%b
echo Detected unity version: %VERSION_REST%

set UNITY_VERSION=%VERSION_REST%
:skip_version_parse
set UNITY_PATH=C:\Program Files\Unity\Hub\Editor\%UNITY_VERSION%\Editor\Unity.exe

if not exist "%UNITY_PATH%" (
	echo ERROR: Did not find unity installation at path: %UNITY_PATH%
	goto :error_build
)

rem Run unit tests
rem "%UNITY_PATH%" -batchmode -projectPath "%PROJECT_PATH%" -logFile "%TEST_OPS%" -runTests -testResults "%TEST_RESULTS%" || goto :error_unit
"%UNITY_PATH%" -batchmode -projectPath "%PROJECT_PATH%" -logFile "%TEST_OPS%" -executeMethod TestRunner.RunEditorUnitTests -testResults "%TEST_RESULTS%" || goto :error_unit

rem Run the build
"%UNITY_PATH%" -quit -batchmode -strictMode -logFile "%BUILD_OPS%" -silent-crashes -projectPath "%PROJECT_PATH%" -buildWindows64Player "%BUILD_PATH%" || goto :error_build

rem Success
echo "Build success"
echo -------- UNIT TESTS OK -----------
type "%TEST_OPS%"
echo --------- BUILD OK ----------
type "%BUILD_OPS%"
exit /b

:error_unit
echo "Build failed"
echo --------- UNIT TESTS FAILED ----------
type "%TEST_OPS%"
exit /b 1

:error_build
echo "Build failed"
echo --------- BUILD ERROR ----------
type "%BUILD_OPS%"
exit /b 1